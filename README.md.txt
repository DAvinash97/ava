# AvaBot
A modular telegram Python bot running on python3 with an sqlalchemy database.

Originally a Marie fork, Pɧơɛnıх was created for personal use by [this person](https://t.me/DAvinash97). Feel free to add it to your groups though!

Can be found on telegram as [AvaBot](https://t.me/DAvinash97bot).

If you want to create your own bot, you can basically follow the steps given [here](https://github.com/PaulSonOfLars/tgbot/blob/master/README.md)
Special thanks to rsktg for this bot and his hardwork for the modules, i just used his sources.

## Credits
• [SkittBot](https://github.com/skittles9823/SkittBot) for stickers module.
• [SaitamaRobot](https://github.com/AnimeKaizoku/SaitamaRobot) for evaluator and more.
• MrYacha, Ayra Hikari and Mizukito Akito for Federations.
• 1maverick1 for welcome mutes.

For the link to the old repo containing all the commit history and authorships, [click here](https://github.com/rsktg/Phoenix.git)

## Notice
No new features will be added to this project anymore, however occasional bug fixes will be released.
